<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pembelian;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pembelian = pembelian::all();
        return view('pembelian.index', compact('pembelian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('pembelian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'barang' => 'required|max:255',
            'jumlah' => 'required',
            'deskripsi' => 'required',
        ]);
        
        $pembelian = pembelian::create([
            'barang' => $request['barang'],
            'jumlah' => $request['jumlah'],
            'deskripsi' => $request['deskripsi'],
        ]);
        return redirect('/pembelian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pembelian = pembelian::find($id);

        return view('pembelian.pembelian', compact('pembelian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pembelian = pembelian::find($id);
        return view('pembelian.edit', compact('pembelian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'barang' => 'required|max:255',
            'jumlah' => 'required',
            'deskripsi'  => 'required|max:255',
        ]);

        $pembelian = pembelian::where('id',$id) -> update([
            'barang' => $request['barang'],
            'jumlah' => $request['jumlah'],
            'deskripsi' => $request['deskripsi'],
        ]);
        return redirect ('/pembelian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pembelian::destroy($id);
        return redirect ('/pembelian');
    }
}
