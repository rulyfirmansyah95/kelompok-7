<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index(Request $request){
    	$user=DB::table('users')->get();
    	return view('user',['data'=>$user]);
    }

    public function create(Request $request){

    }

    public function store(Request $request){
    	$validated = $request->validate([
    		'name' => 'required',
    		'email' => 'required',
            'password' => 'required',
    	]);
    	$data=[
    		'name' => $request->name,
    		'email' => $request->email,
            'password' => Hash::make($request->password),
    	];
    	$insert=DB::table('users')->insert($data);
    	return Redirect::to('/user')->with('success', 'Data Berhasil ditambahkan');
    }

    public function update(Request $request,$id){
    	$validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $data=[
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ];
    	
    	$action=DB::table('users')->where('id',$id)->update($data);
    	return Redirect::to('/user')->with('success', 'Data Berhasil di perbarui');
    }

    public function destroy(Request $request,$id){
    	$action=DB::table('users')->where('id',$id)->delete();
    	return Redirect::to('/user')->with('success', 'Data Berhasil di hapus');
    }


}
