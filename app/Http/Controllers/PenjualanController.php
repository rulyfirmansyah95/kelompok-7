<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\penjualan;

class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penjualan = penjualan::all();
        return view ('penjualan.index', compact('penjualan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penjualan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'barang' => 'required|max:255',
            'jumlah' => 'required',
            'deskripsi'  => 'required|max:255',
        ]);
        
        $penjualan = penjualan::create([
            'barang' => $request['barang'],
            'jumlah' => $request['jumlah'],
            'deskripsi' => $request['deskripsi'],
        ]);
        return redirect('/penjualan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penjualan = penjualan::find($id);
        return view('penjualan.penjualan', compact('penjualan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penjualan = penjualan::find($id);
        return view ('penjualan.edit', compact('penjualan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penjualan = penjualan::where('id',$id) -> update([
            'barang' => $request['barang'],
            'jumlah' => $request['jumlah'],
            'deskripsi' => $request['deskripsi'],
        ]);
        return redirect ('/penjualan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        penjualan::destroy($id);
        return redirect ('/penjualan');
    }
}
