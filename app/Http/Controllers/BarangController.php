<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class BarangController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function index(Request $request){
    	$barang=DB::table('barang')->get();
    	return view('barang',['data'=>$barang]);
    }

    public function create(Request $request){
		
    }

    public function store(Request $request){
    	$validated = $request->validate([
    		'nama' => 'required',
    		'harga' => 'required',
    	]);
    	$data=[
    		'nama_barang' => $request->nama,
    		'harga' => $request->harga,
    	];
    	$insert=DB::table('barang')->insert($data);
    	return Redirect::to('/barang')->with('success', 'Data Berhasil ditambahkan');
    }

    public function show(Request $request,$id){

    }

    public function edit(Request $request,$id){

    }

    public function update(Request $request,$id){
    	$validated = $request->validate([
    		'nama' => 'required',
    		'harga' => 'required',
    	]);
    	$data=[
    		'nama_barang' => $request->nama,
    		'harga' => $request->harga,
    	];
    	
    	$action=DB::table('barang')->where('id',$id)->update($data);
    	return Redirect::to('/barang')->with('success', 'Data Berhasil di perbarui');
    }

    public function destroy(Request $request,$id){
    	$action=DB::table('barang')->where('id',$id)->delete();
    	return Redirect::to('/barang')->with('success', 'Data Berhasil di hapus');
    }


}
