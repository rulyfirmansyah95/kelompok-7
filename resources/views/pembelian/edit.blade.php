@extends('layouts-LTE.app')

@section('content')
<div>
    <form action="/pembelian/{{$pembelian->id}}" method="POST" enctype="multipart/form-data">
		@csrf
        @method('put')
		<div class="form-group">
			<label>Barang</label><br>
			<input type="text" class="form-control" name="barang" value="{{$pembelian->barang}}"> <br><br>
			@error('barang')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
			@enderror
		</div>
		<div class="form-group">
			<label>Jumlah</label><br>
			<input type="text" class="form-control" name="jumlah" value="{{$pembelian->jumlah}}"> <br><br>
			@error('jumlah')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
			@enderror
		</div>
		<div class="form-group">
			<label>Deskripsi</label> <br>
			<textarea name='deskripsi' class="form-control" cols="30" rows="10">{{$pembelian->deskripsi}}</textarea> <br><br>
			@error('deskripsi')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
			@enderror
		</div>
		<button type="submit" class="btn btn-primary">Tambah</button>
	</form>
</div>



@endsection