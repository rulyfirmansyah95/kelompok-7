@extends('layouts-LTE.app')

@section('content')

<a href="/pembelian/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Barang</th>
        <th scope="col">Jumlah</th>
		<th scope="col">Deskripsi</th>
		<th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($pembelian as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->barang}}</td>
                <td>{{$value->jumlah}}</td>
				<td>{{$value->deskripsi}}</td>
                <td>
                    <a href="/pembelian/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/pembelian/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/pembelian/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>

@endsection