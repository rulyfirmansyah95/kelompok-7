@extends('layouts-LTE.app')

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Master User</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Master User</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">

		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Data User</h3>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="col-md-12">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add">
						<i class="fas fa-plus"></i> Tambah User
					</button>
				</div>

				<br>

				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama User</th>
							<th>Email</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						@foreach($data as $user)
						

						<tr>
							<td>{{ $loop->index+1 }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td>
								
								<button class="btn btn-info" data-toggle="modal" data-target="#modal-edit-{{ $user->id }}"><i class="fas fa-edit"></i></button>
								<button class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $user->id }}"><i class="fas fa-trash"></i></button>
							</td>
						</tr>

						<div class="modal fade" id="modal-edit-{{ $user->id }}">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Ubah User</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form action="{{ url('user/'.$user->id) }}" method="post" autocomplete="off">
										@method('put')
										@csrf
										<input type="hidden" name="id" value="{{ $user->id }}">
										<div class="modal-body">

											<div class="form-group">
												<label for="exampleInputEmail1">Nama User</label>
												<input type="text" name="name" class="form-control" placeholder="Nama User" value="{{ $user->name }}">
											</div>

											<div class="form-group">
												<label for="exampleInputEmail1">Email</label>
												<input type="email" name="nama" class="form-control" placeholder="Email User" value="{{ $user->email }}">
											</div>

										</div>
										<div class="modal-footer justify-content-between">
											<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-primary">Simpan</button>
										</div>
									</form>
								</div>
							</div>
						</div>


						<div class="modal fade" id="modal-delete-{{ $user->id }}">
							<div class="modal-dialog">
								<div class="modal-content bg-warning">
									<div class="modal-header">
										<h4 class="modal-title">Hapus User</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form action="{{ url('user/'.$user->id) }}" method="post" autocomplete="off">
										@method('delete')
										@csrf
										<input type="hidden" name="id" value="{{ $user->id }}">
										<div class="modal-body">
											<p align="center">Apakah Anda Yakin Ingin menghapus User dengan nama {{ $user->name }} ?</p>

										</div>
										<div class="modal-footer justify-content-between">
											<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-danger">Hapus</button>
										</div>
									</form>
								</div>
							</div>
						</div>

						@endforeach


					</tbody>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Nama User</th>
							<th>Email User</th>
							<th>Aksi</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.card-body -->
		</div>

	</div>
	<!-- /.row (main row) -->
</div>

</section>
<!-- /.content -->

<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah User</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ route('user.store') }}" method="post" autocomplete="off">
				@csrf
				<div class="modal-body">

					<div class="form-group">
						<label for="exampleInputEmail1">Nama User</label>
						<input type="text" name="name" class="form-control" placeholder="Nama User" required>
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Email User</label>
						<input type="email" name="email" class="form-control" placeholder="Email" required>
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Password</label>
						<input type="password" name="password" min="8" class="form-control" placeholder="Password" required>
					</div>

				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>





@endsection