@extends('layouts-LTE.app')

@section('content')
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0">Master Barang</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
					<li class="breadcrumb-item active">Master Barang</li>
				</ol>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">

		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Data Barang</h3>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="col-md-12">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add">
						<i class="fas fa-plus"></i> Tambah Barang
					</button>
				</div>

				<br>

				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama Barang</th>
							<th>Harga Barang</th>
							<th>Stok Tersedia (Pcs)</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						@foreach($data as $barang)
						

						<tr>
							<td>{{ $loop->index+1 }}</td>
							<td>{{ $barang->nama_barang }}</td>
							<td>{{ number_format($barang->harga) }}</td>
							<td>{{ $barang->stok }}</td>
							<td>
								
								<button class="btn btn-info" data-toggle="modal" data-target="#modal-edit-{{ $barang->id }}"><i class="fas fa-edit"></i></button>
								<button class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $barang->id }}"><i class="fas fa-trash"></i></button>
							</td>
						</tr>

						<div class="modal fade" id="modal-edit-{{ $barang->id }}">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Ubah Barang</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form action="{{ url('barang/'.$barang->id) }}" method="post" autocomplete="off">
										@method('put')
										@csrf
										<input type="hidden" name="id" value="{{ $barang->id }}">
										<div class="modal-body">

											<div class="form-group">
												<label for="exampleInputEmail1">Nama Barang</label>
												<input type="text" name="nama" class="form-control" placeholder="Nama Barang" value="{{ $barang->nama_barang }}">
											</div>

											<div class="form-group">
												<label for="exampleInputEmail1">Harga Barang</label>
												<input type="number" name="harga" min="0" class="form-control" placeholder="Harga" value="{{ $barang->harga }}">
											</div>

										</div>
										<div class="modal-footer justify-content-between">
											<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-primary">Simpan</button>
										</div>
									</form>
								</div>
							</div>
						</div>


						<div class="modal fade" id="modal-delete-{{ $barang->id }}">
							<div class="modal-dialog">
								<div class="modal-content bg-warning">
									<div class="modal-header">
										<h4 class="modal-title">Hapus Barang</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<form action="{{ url('barang/'.$barang->id) }}" method="post" autocomplete="off">
										@method('delete')
										@csrf
										<input type="hidden" name="id" value="{{ $barang->id }}">
										<div class="modal-body">
											<p align="center">Apakah Anda Yakin Ingin menghapus barang dengan nama {{ $barang->nama_barang }} ?</p>

										</div>
										<div class="modal-footer justify-content-between">
											<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
											<button type="submit" class="btn btn-danger">Hapus</button>
										</div>
									</form>
								</div>
							</div>
						</div>

						@endforeach


					</tbody>
					<tfoot>
						<tr>
							<th>#</th>
							<th>Nama Barang</th>
							<th>Harga Barang</th>
							<th>Stok Tersedia (Pcs)</th>
							<th>Aksi</th>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- /.card-body -->
		</div>

	</div>
	<!-- /.row (main row) -->
</div>

</section>
<!-- /.content -->

<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Barang</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ route('barang.store') }}" method="post" autocomplete="off">
				@csrf
				<div class="modal-body">

					<div class="form-group">
						<label for="exampleInputEmail1">Nama Barang</label>
						<input type="text" name="nama" class="form-control" placeholder="Nama Barang">
					</div>

					<div class="form-group">
						<label for="exampleInputEmail1">Harga Barang</label>
						<input type="number" name="harga" min="0" class="form-control" placeholder="Harga">
					</div>

				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>





@endsection