@extends('layouts-LTE.app')

@section('content')
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0">Dashboard</h1>
						</div><!-- /.col -->
						<div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="#">Home</a></li>
								<li class="breadcrumb-item active">Dashboard</li>
							</ol>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
				<div class="container-fluid" align="center">

					<img src="images/logo.svg" width="60%">

					<h1 align="center">Kelompok 7</h1>
					<h3 align="center">Final Project</h3>

					<div class="row">
						<div class="col-lg-4 col-6">
							<!-- small box -->
							<div class="small-box bg-info">
								<div class="inner">
									<h3>5</h3>

									<p>Data Barang</p>
								</div>
								<div class="icon">
									<i class="ion ion-bag"></i>
								</div>
								<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
						<div class="col-lg-4 col-6">
							<!-- small box -->
							<div class="small-box bg-success">
								<div class="inner">
									<h3>N/A</h3>

									<p>Penjualan</p>
								</div>
								<div class="icon">
									<i class="ion ion-stats-bars"></i>
								</div>
								<a href="/penjualan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
						<div class="col-lg-4 col-6">
							<!-- small box -->
							<div class="small-box bg-warning">
								<div class="inner">
									<h3>N/A</h3>

									<p>Pembelian</p>
								</div>
								<div class="icon">
									<i class="ion ion-person-add"></i>
								</div>
								<a href="/pembelian" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
					</div>
					
				</div>
				<!-- /.row (main row) -->
			</div>
			
		</section>
		<!-- /.content -->



@endsection